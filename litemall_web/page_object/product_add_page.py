"""
__author__ = '霍格沃兹测试开发学社'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
from selenium.webdriver.common.by import By

from litemall_web.page_object.base_page import BasePage
from litemall_web.page_object.product_list_page import ProductListPage


class ProductAddPage(BasePage):
    _ADD_PRODUCT_BUTTON = (By.CSS_SELECTOR, ".op-container .el-button--primary")

    def add_product(self, goods_sn, goods_name):
        self.find(By.CSS_SELECTOR, ".el-form-item:nth-child(1) input").send_keys(goods_sn)
        self.find(By.CSS_SELECTOR, ".el-form-item:nth-child(2) input").send_keys(goods_name)
        self.find(self._ADD_PRODUCT_BUTTON).click()
        return ProductListPage(self.driver)
