"""
__author__ = '霍格沃兹测试开发学社'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver


class BasePage:
    def __init__(self, base_driver: WebDriver=None):
        if base_driver == None:
            self.driver = webdriver.Chrome()
            # 添加显示等待配置
            self.driver.implicitly_wait(3)
            self.driver.get("https://litemall.hogwarts.ceshiren.com/")
            self.driver.maximize_window()
        else:
            self.driver = base_driver


    def find(self, by, locator=None):
        print(f"查找的元素为{by}-{locator}")
        if locator:
            return self.driver.find_element(by, locator)
        else:
            return self.driver.find_element(*by)

    def finds(self, by, locator=None):
        print(f"查找的元素为{by}-{locator}")
        if locator:
            return self.driver.find_elements(by, locator)
        else:
            return self.driver.find_elements(*by)

    def goto_url_address(self, url):
        """
        跳转到某个页面
        :param url:
        :return:
        """
        self.driver.get(url)

    def quit(self):
        self.driver.quit()