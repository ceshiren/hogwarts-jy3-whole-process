"""
__author__ = '霍格沃兹测试开发学社'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
from selenium.webdriver.common.by import By

from litemall_web.page_object.base_page import BasePage
from litemall_web.page_object.dashboard_page import DashboardPage


class LoginPage(BasePage):
    def login_by_account(self, username, password):
        # print("xxxx")
        # self.driver.find_element(By.NAME, "username")
        self.find(By.NAME, "username").clear()
        self.find(By.NAME, "username").send_keys(username)
        self.find(By.NAME, "password").clear()
        self.find(By.NAME, "password").send_keys(password)
        self.find(By.CSS_SELECTOR, "button").click()


        return DashboardPage(self.driver)
