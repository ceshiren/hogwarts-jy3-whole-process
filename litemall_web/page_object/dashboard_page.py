"""
__author__ = '霍格沃兹测试开发学社'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
import time

from selenium.webdriver.common.by import By

from litemall_web.page_object.base_page import BasePage
from litemall_web.page_object.product_add_page import ProductAddPage


class DashboardPage(BasePage):
    def goto_product_add_page(self):
        time.sleep(1)
        self.find(By.XPATH, "//*[text()='商品管理']").click()
        self.find(By.XPATH, "//*[text()='商品上架']").click()
        return ProductAddPage(self.driver)