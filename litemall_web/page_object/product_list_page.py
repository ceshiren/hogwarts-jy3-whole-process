"""
__author__ = '霍格沃兹测试开发学社'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
import time

from selenium.webdriver.common.by import By

from litemall_web.page_object.base_page import BasePage

class ProductListPage(BasePage):
    def get_product_list(self):
        pass

    def search_product_by_goods_sn(self, goods_sn):
        """
        通过商品编号进行搜索
        :param goods_sn:
        :return:
        """

        self.find(By.CSS_SELECTOR, "[placeholder='请输入商品编号']").send_keys(goods_sn)
        self.find(By.CSS_SELECTOR, ".el-icon-search").click()
        time.sleep(1)
        eles = self.finds(By.CSS_SELECTOR, "td:nth-child(3)")
        # print(eles)
        product_name_list = [ele.text for ele in eles]
        return product_name_list

