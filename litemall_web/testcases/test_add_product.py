"""
__author__ = '霍格沃兹测试开发学社'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
import pytest

from litemall_web.page_object.login_page import LoginPage


class TestAddProduct():
    def setup_class(self):
        # self.login_page = LoginPage(
        self.dashboard_page = LoginPage().\
            login_by_account("admin123", "admin123")

    # todo: 1. 完成添加商品的参数化的过程
    @pytest.mark.parametrize("sn_number, product_name",
                             [("13160678", "hogwarts"),
                              ("13160679", "hogwarts2")])
    def test_add_product(self, sn_number, product_name):
        # 1. 打开页面 用户登录 2.点开菜单 3.上架商品 4.检验结果
        name_list = self.dashboard_page.goto_product_add_page().\
            add_product(sn_number, product_name).\
            search_product_by_goods_sn(sn_number)
        assert product_name in name_list

    def teardown(self):
        self.dashboard_page.goto_url_address(
            "https://litemall.hogwarts.ceshiren.com/#/dashboard")


    def teardown_class(self):
        self.dashboard_page.quit()
