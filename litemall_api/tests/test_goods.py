"""
__author__ = '霍格沃兹测试开发学社'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
import allure
from jsonpath import jsonpath

from litemall_api.apis.goods_api import GoodsApi


class TestGoods():
    def setup_class(self):
        self.goods = GoodsApi()

    @allure.story("新增用例")
    def test_add_goods(self):
        r1 = self.goods.create(goods_sn="13199991111", goods_name="货品上架")
        r2 = self.goods.get("货品上架", "13199991111")
        res = jsonpath(r2, "$..name")
        goods_id = jsonpath(r2, "$..id")[0]
        self.goods.delete(goods_id)
        assert r1.get("errmsg") == "成功"
        assert res[0] == "货品上架"


    @allure.story("查询测试用例")
    def test_get_goods(self):
        r = self.goods.get("hogwartsad", "1314")
        res = jsonpath(r, "$..name")
        assert res[0] == "hogwartsad"

