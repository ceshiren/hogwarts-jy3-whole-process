"""
__author__ = '霍格沃兹测试开发学社'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
import requests

from litemall_api.utils.log_utils import logger


class LitemallApi:
    # 优化点： 1. headers 的token 重复
    # 2. 日志信息重复(封装父类的send方法，子类统一调用)
    def __init__(self):
        # 如何切换环境
        # env_config = Utils.get_yaml_data("env")
        # default_env = env_config["default"]
        self.base_url = "https://litemall.hogwarts.ceshiren.com"

    def __set_token(self, requests_info):
        """
        :param requests_info: 接口请求的其他信息 kwargs
        :return: 接口请求的其他信息 kwargs + 塞入的token信息
        """
        # 避免重复获取token


        if not hasattr(self, "token"):
            info = {"username":"admin123","password":"admin123","code":""}
            r = requests.post(self.base_url+"/admin/auth/login", json=info)
            self.token = r.json()["data"]["token"]

        if requests_info.get("headers"):
            requests_info["headers"].update({"X-Litemall-Admin-Token": self.token})
        else:
            requests_info["headers"] = {"X-Litemall-Admin-Token": self.token}
        return requests_info

    def send(self, method, url, **kwargs):
        """
        针对requests 做二次封装。 日志信息、响应格式规范
        :return:
        """
        # 在发起请求之前，将token信息塞入到头信息=> 1.头信息在哪里？ 2. 怎么塞
        kwargs = self.__set_token(kwargs)
        r = requests.request(method, self.base_url + url, **kwargs)
        logger.debug(f"{url}响应值信息为{r.json()}")
        return r.json()

