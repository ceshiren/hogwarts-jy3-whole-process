"""
__author__ = '霍格沃兹测试开发学社'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
from litemall_api.apis.litemall_api import LitemallApi


class GoodsApi(LitemallApi):

    # 上架商品
    def create(self, goods_sn: str, goods_name: str):
        # logger.info(f"商品编号：{goods_sn}, 商品名称：{goods_name}")
        url = f"/admin/goods/create"
        data = {"goods": {"goodsSn": goods_sn, "name": goods_name, }, "specifications": [
            {
                "specification": "规格",
                "value": "标准",
                "picUrl": ""
            }
        ], "products": [
            {
                "id": 0,
                "specifications": [
                    "标准"
                ],
                "price": 0,
                "number": 0,
                "url": ""
            }

        ], "attributes": [{"attribute": "材质", "value": "纯棉"}]}

        r = self.send("post", url, json=data)
        return r

    def get(self, goods_name, goods_sn):
        url = "/admin/goods/list"
        params= {"name": goods_name, "goodsSn": goods_sn}
        r = self.send("get", url, params=params)
        return r

    def delete(self, goods_id):
        url = "/admin/goods/delete"
        data = {"id": goods_id}
        r = self.send("post", url, json=data)
        return r
